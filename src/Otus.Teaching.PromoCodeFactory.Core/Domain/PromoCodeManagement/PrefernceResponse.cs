﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class PrefernceResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
