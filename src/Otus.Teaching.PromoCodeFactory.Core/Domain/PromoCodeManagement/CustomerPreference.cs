﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class CustomerPreference
    {
        public Guid CustumerId { get; set; }
        public Guid PreferenceId { get; set; }

        public Customer Customer { get; set; }
        public Preference Preference { get; set; }

    }
}
