﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T: BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();
        
        Task<T> GetByIdAsync(Guid id);

        Task<IEnumerable<T>> GetRangeAsync(List<Guid> Ids);

        Task<Guid> CreateAsync(T item);

        Task RemoveAsync(Guid Id);

        Task UpdateAsync(T entity);

        Task<Preference> GetPreferenceByNameAsyncUpdateAsync(string name);
    }
}