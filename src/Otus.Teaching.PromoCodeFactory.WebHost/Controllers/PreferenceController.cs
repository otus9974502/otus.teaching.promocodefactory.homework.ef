﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PreferenceController : ControllerBase
    {
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IMapper _mapper;

        public PreferenceController(IRepository<Preference> preferenceRepository, IMapper mapper)
        {
            _preferenceRepository = preferenceRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// Получить все предпочтения
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var items = await _preferenceRepository.GetAllAsync();
            return Ok(_mapper.Map<List<PrefernceResponse>>(items));
        }

        /// <summary>
        /// Создать предпочтение
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<Guid> CreateAsync(CreateOrUpdatePreference request)
        {
            var item = await _preferenceRepository.CreateAsync(new Preference()
            {
                Name = request.Name
            });
            return item;
        }

        [HttpPut("{Id}")]
        public async Task<IActionResult> UpdateAsync(Guid Id, CreateOrUpdatePreference request)
        {
            var item = await _preferenceRepository.GetByIdAsync(Id);
            if(item != null)
            {
                item.Name = request.Name;
                await _preferenceRepository.UpdateAsync(item);
            }

            return NotFound();
        }

        [HttpDelete("{Id}")]
        public async Task<IActionResult> DeleteAsync(Guid Id)
        {
            await _preferenceRepository.RemoveAsync(Id);
            return Ok();
        }
    }
}
