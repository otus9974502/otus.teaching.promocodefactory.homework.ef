﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IMapper _mapper;

        public CustomersController(IRepository<Customer> customerRepo, IRepository<Preference> preferenceRepo, IMapper mapper) { 
            _customerRepository = customerRepo;
            _preferenceRepository = preferenceRepo;
            _mapper = mapper;
        }

        /// <summary>
        /// получить всех клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
        {
            var list = await _customerRepository.GetAllAsync();
            var resp = _mapper.Map<List<CustomerShortResponse>>(list);

            return Ok(resp);
        }
        
        /// <summary>
        /// получить клиента по id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            //TODO: Добавить получение клиента вместе с выданными ему промомкодами
            var item = await _customerRepository
                .GetByIdAsync(id);
            return Ok(item);
        }
        
        /// <summary>
        /// создать нового клиента
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            //TODO: Добавить создание нового клиента вместе с его предпочтениями
            var prefs = await _preferenceRepository.GetRangeAsync(request.PreferenceIds);

            var customer = new Customer()
            {
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName
            };
            customer.Preferences = prefs.Select(prefs => new CustomerPreference()
            {
                Customer = customer,
                PreferenceId = prefs.Id,
            }).ToList();


            var id = _customerRepository.CreateAsync(customer);

            return Ok(id);
        }
        
        /// <summary>
        /// обновить данные клиента
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            //TODO: Обновить данные клиента вместе с его предпочтениями
            var item = await _customerRepository.GetByIdAsync(id);
            if(item == null)
                return NotFound();

            var prefs = await _preferenceRepository.GetRangeAsync(request.PreferenceIds);

            if (item != null)
            {
                item.FirstName = request.FirstName;
                item.LastName = request.LastName;
                item.Email = request.Email;
                
                item.Preferences = prefs.Select(prefs => new CustomerPreference()
                {
                    Customer = item,
                    PreferenceId = prefs.Id,
                }).ToList();
                ;
            }
            await _customerRepository.UpdateAsync(item);

            return Ok(item);
        }
        
        /// <summary>
        /// удалить клиента
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            //TODO: Удаление клиента вместе с выданными ему промокодами
            await _customerRepository.RemoveAsync(id);
            return Ok();
        }
    }
}