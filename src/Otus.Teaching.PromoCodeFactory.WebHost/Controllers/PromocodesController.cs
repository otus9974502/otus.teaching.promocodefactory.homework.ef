﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> _promoRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<Employee> _employeeRepository;

        private readonly IMapper _mapper;

        public PromocodesController(IRepository<PromoCode> repository, IRepository<Preference> preferenceRepository, IRepository<Employee> employeeRepository, IMapper mapper)
        {
            _promoRepository = repository;
            _preferenceRepository = preferenceRepository;
            _employeeRepository = employeeRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            //TODO: Получить все промокоды 
            var list = await _promoRepository.GetAllAsync();
            return Ok(_mapper.Map<List<PromoCodeShortResponse>>(list));
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            //TODO: Создать промокод и выдать его клиентам с указанным предпочтением
            //var pref = await _preferenceRepository.GetPreferenceByNameAsyncUpdateAsync(request.Preference);

            //if(pref == null)
            //    return NotFound("Preference not found");

            //var employers = await _employeeRepository.GetEmployersByPreferenceId(pref.Id);
            //if (employers == null || employers.Count <= 0)
            //    return NotFound("Partner not found");


            //var beginDate = DateTime.Now;
            //await _promoRepository.CreateAsync(new PromoCode() { 
            //    BeginDate = beginDate,
            //    EndDate = beginDate.AddDays(10),
            //    Code = request.PromoCode,
            //    ServiceInfo = request.ServiceInfo,
            //    Preference = pref,
            //    PartnerManager = ,
            //    PartnerName = request.PartnerName,
            //});

            return BadRequest(request);
        }
    }
}