﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T> where T : BaseEntity
    {
        protected SqLiteDbContext _db;

        public EfRepository(SqLiteDbContext data)
        {
            _db = data;
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _db.Set<T>().ToListAsync();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return await _db.Set<T>().FirstOrDefaultAsync(i => i.Id == id);
        }

        public async Task<Guid> CreateAsync(T item)
        {
            await _db.Set<T>().AddAsync(item);
            await _db.SaveChangesAsync();
            return item.Id;
        }

        public async Task<IEnumerable<T>>GetRangeAsync(List<Guid> Ids)
        {
            var list = await _db.Set<T>().Where(i => Ids.Contains(i.Id)).ToListAsync();
            return list;
        }

        public async Task RemoveAsync(Guid Id)
        {
            var item = await _db.Set<T>().FindAsync(Id);
            if (item != null)
            {
                _db.Set<T>().Remove(item);
                await _db.SaveChangesAsync();
            }
        }

        public async Task UpdateAsync(T entity)
        {
            await _db.SaveChangesAsync();
        }

        public async Task<Preference> GetPreferenceByNameAsyncUpdateAsync(string name)
        {
            var item = await _db.Preferences.FirstOrDefaultAsync(i => i.Name.Equals(name));
            return item;
        }
    }
}
