﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Context
{
    public class SqLiteDbContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Filename=database.db");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PromoCode>()
                .HasOne(p => p.Preference);

            modelBuilder.Entity<Employee>()
                .HasOne(p => p.Role);

            modelBuilder.Entity<Customer>()
                .HasMany(p => p.Preferences);

            modelBuilder.Entity<Customer>()
                .HasMany(p => p.Promocodes);


            modelBuilder.Entity<CustomerPreference>()
                .HasKey(i => new { i.CustumerId, i.PreferenceId });
            
            modelBuilder.Entity<CustomerPreference>()
                .HasOne(i => i.Customer)
                .WithMany(i => i.Preferences)
                .HasForeignKey(i => i.CustumerId);
            
            modelBuilder.Entity<CustomerPreference>()
                .HasOne(i => i.Preference)
                .WithMany()
                .HasForeignKey(i => i.PreferenceId);
            

            base.OnModelCreating(modelBuilder);
        }
    }
}
