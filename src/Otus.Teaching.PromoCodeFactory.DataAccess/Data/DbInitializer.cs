﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions;
using Otus.Teaching.PromoCodeFactory.DataAccess.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class DbInitializer : IDbInitializer
    {
        private readonly SqLiteDbContext _context;

        public DbInitializer(SqLiteDbContext dataContext)
        {
            _context = dataContext;
        }

        public void UpdateDb()
        {
            _context.Database.EnsureDeleted();
            _context.Database.EnsureCreated();

            _context.AddRange(FakeDataFactory.Employees);
            _context.AddRange(FakeDataFactory.Preferences);
            _context.AddRange(FakeDataFactory.Customers);
            _context.SaveChanges();
        }
    }
}
